<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StrahovkasController;
use App\Http\Controllers\AdminStrahovkaController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('strahovkas', [StrahovkasController::class, 'index']);
Route::get('strahovkas/{strahovka}', [StrahovkasController::class, 'strahovka']);

Route::resource('/admin/strahovkas', AdminStrahovkaController::class);
