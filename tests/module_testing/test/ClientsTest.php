<?php

use Tests\module_testing\src\Clients;

class ClientsTest extends \PHPUnit\Framework\TestCase
{

    public function testSecondName(){

        $second_name = new Clients;
        $second_name->second_name = 'Bulba';
        $this->assertEquals('Bulba', $second_name->getSecondName());

    }

    public function testFirstName(){

        $first_name = new Clients;
        $first_name->first_name = 'Vlad';
        $this->assertEquals('Vlad', $first_name->getFirstName());

    }

    protected function setUp(): void
    {
        $this->clients = new Clients;
    }

    /**
     * @dataProvider clientsDataProvider
     * @param $actual
     * @param $expected
     */

    public function testClients($actual, $expected){
        $result = $this->clients = $actual;
        $this->assertEquals($expected, $result);
    }

    public function clientsDataProvider(): array{
        return array(
            array(150, 151),
            array(103, 103),
            array(85, 85),
            array(333, 333)
        );
    }

    public function testClientsOneMore()
    {
        $stub = $this
            ->getMockBuilder(Clients::class)
            ->disableOriginalConstructor()
            ->getMock();

        $stub->method('getSecondName')
            ->willReturn('Popova');

        $this->assertSame('Popova', $stub->getSecondName());

    }
}
