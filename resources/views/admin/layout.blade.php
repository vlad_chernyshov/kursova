<h1>Адмінка</h1>

<div class="leftnav" style="float: left; width: 150px; height: 100%; margin-right:
30px;">

    <b>Страховки</b>
    <ul>
        <li><a href="/admin/strahovkas">список</a></li>
        <li><a href="/admin/strahovkas/create">додати</a></li>
    </ul>
    <br/>

    <b>Кабінет</b>
    <ul>
        <li><a href="/logout">Logout</a></li>
    </ul>
    <ul>
        <li><a href="/strahovkas">Frontend</a></li>
    </ul>
</div>

<div>
    @yield('content')
</div>
