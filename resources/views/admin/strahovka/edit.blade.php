@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')

    <h2>Редагування страховки</h2>
    <form action="/admin/strahovkas/{{ $strahovka->StrahovkaID }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}

        <label>Страховий поліс</label>
        <input type="text" name="StrahovkaName" value="{{$strahovka->StrahovkaName}}">
        <br/><br/>
        <label>Вартість полісу</label>
        <input type="text" name="StrahovkaCost" value="{{$strahovka->StrahovkaCost}}">
        <br/><br/>
        <label>Термін дії</label>
        <select name="type">
            @foreach($strahovkas_types as $type_id => $type_title)
                <option value="{{ $type_id }}"
                    {{ ( $type_id == $strahovka->type ) ? 'selected' : '' }}>
                    {{ $type_title }}
                </option>
            @endforeach
        </select>
        <br/>
        <br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
