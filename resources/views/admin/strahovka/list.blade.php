@extends('admin.layout')
@section('content')

    <h2>Список </h2>

    <table border="1" style="text-align: center;">
        <th>Страховий поліс</th>
        <th>Вартість полісу</th>
        <th>Термін дії</th>
        <th>Дія</th>
        @foreach ($strahovkas as $strahovka)
            <tr>
                <td>
                    <a href="/strahovka/{{ $strahovka->StrahovkaID }}">{{ $strahovka->StrahovkaName}}</a>
                </td>
                <td>{{ $strahovka->StrahovkaCost }}</td>
                <td>{{ $strahovka->state }}</td>
                <td>
                    <a href="/admin/strahovkas/{{ $strahovka->StrahovkaID }}/edit">edit</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/strahovkas/{{ $strahovka->StrahovkaID }}"method="POST">
                        {{ method_field('DELETE') }}

                        {{ csrf_field() }}
                        <button>Delete</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
