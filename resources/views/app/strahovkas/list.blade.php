@extends('app.layouts.layout')
@section('page_title')

    <b>Список страховок</b>

@endsection

<?php
$model_strahovkas = new \App\Models\Strahovkas();
$stoimost = $model_strahovkas->getStrahovkaCost();
?>



@section('content')
    <form method="get" action="/strahovkas">

        <select name=" type">
            <option value="0">всі категорії</option>
            @foreach($strahovka_types as $type_id => $type_title)
                <option value="{{ $type_id }}"
                    {{ ( $type_id == $type_selected ) ? 'selected' : '' }}>
                    {{ $type_title }}
                </option>
            @endforeach

        </select>


        <select name=" StrahovkaCost">
            <option value="0">всі ціни</option>
            @foreach($stoimost as $cost)
                <option value="{{ $cost->StrahovkaCost }}"
                    {{ ( $cost->StrahovkaCost  == $cost_selected ) ? 'selected' : '' }}>
                    {{ $cost->StrahovkaCost  }}
                </option>
            @endforeach

        </select>
        <input type="submit" value="Знайти" />
    </form>
    <table>
        <th>Страховий поліс</th>
        <th>Вартість полісу</th>
        <th>Термін дії</th>
        <th>Страхові випадки</th>
        @foreach ($strahovkas as $strahovka)
            <tr>
                <td>
                    <a href="/strahovkas/{{ $strahovka->StrahovkaID }}">

                        {{ $strahovka->StrahovkaName }}
                    </a>
                </td>
                <td>{{ $strahovka->StrahovkaCost }}</td>
                <td>{{ $strahovka->state }}</td>
                <td>{{ $strahovka->StrahovkaNumber }}</td>
            </tr>
        @endforeach
    </table>
@endsection
