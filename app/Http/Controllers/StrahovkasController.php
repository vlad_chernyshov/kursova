<?php

namespace App\Http\Controllers;

use App\Models\Strahovkas;
use Illuminate\Http\Request;

class StrahovkasController extends Controller
{

    public function index(Request $request) {

        $type = $request->input('type', null);
        $cost = $request->input('StrahovkaCost', null);

        $model_strahovkas = new Strahovkas();

        $strahovkas = $model_strahovkas->getStrahovkas($type, $cost);

        return view('app.strahovkas.list', [
                'strahovkas' => $strahovkas,
                'strahovka_types' => Strahovkas::$strahovkas_types,
                'type_selected' => $type,
                'cost_selected' => $cost]
        );

    }


    public function strahovka($id) {
        $model_strahovkas = new Strahovkas();
        $strahovka = $model_strahovkas->getStrahovkaByID($id);

        return view('app.strahovkas.strahovka')->with('strahovka', $strahovka);
    }
}
