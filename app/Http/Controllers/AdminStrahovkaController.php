<?php

namespace App\Http\Controllers;


use App\Models\Strahovka;
use App\Models\Strahovkas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdminStrahovkaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $strahovkas = Strahovka::get();
        return view('admin.strahovka.list', ['strahovkas' => $strahovkas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.strahovka.add', ['strahovkas_types' => Strahovkas::$strahovkas_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->input('StrahovkaName');
        $cost = $request->input('StrahovkaCost');
        $type = $request->input('type');

        $strahovka = new Strahovka();
        $strahovka->StrahovkaName = $title;
        $strahovka->state = Strahovkas::$strahovkas_types[$type];
        $strahovka->StrahovkaCost = $cost;
        $strahovka->type = $type;

        $strahovka->save();

        return Redirect::to('/admin/strahovkas');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $strahovka = Strahovka::where('StrahovkaID', $id)->first();

        return view('admin.strahovka.edit', [
            'strahovka' => $strahovka,
            'strahovkas_types' => Strahovkas::$strahovkas_types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $strahovka = Strahovka::where('StrahovkaID', $id)->first();

        $strahovka->StrahovkaName = $request->input('StrahovkaName');
        $strahovka->StrahovkaCost = $request->input('StrahovkaCost');
        $strahovka->type = $request->input('type');

        $strahovka->state = $strahovka->state = Strahovkas::$strahovkas_types[$strahovka->type] ;

        $strahovka->save();
        return Redirect::to('/admin/strahovkas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Strahovka::destroy($id);
        return Redirect::to('/admin/strahovkas');
    }
}
