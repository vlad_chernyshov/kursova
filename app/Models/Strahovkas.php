<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;


class Strahovkas
{
    public static $strahovkas_types = array(
        '1' => 'До першого страхового випадку',
        '2' => 'Річний'
    );

    public function getStrahovkaCost(){
        return DB::select('select distinct(StrahovkaCost) from strahovkas order by StrahovkaCost');
    }


    public function getStrahovkas($type, $cost){
        $query = DB::table('strahovkas')
            ->select('strahovkas.StrahovkaID', 'StrahovkaName', 'state', 'StrahovkaCost','StrahovkaNumber')
            ->join('strahovkanum', 'strahovkas.StrahovkaID', '=', 'strahovkanum.StrahovkaID')
            ->orderBy('StrahovkaID');

        if($type){
            $query->where('type',  $type);
        }

        if($cost){
            $query->where('StrahovkaCost',  $cost);
        }

        $strahovkas = $query->get();

        return $strahovkas;
    }

    public function getStrahovkaByID($id){
        if(!$id) return null;

        $strahovka = DB::table('strahovkas')
            ->select('*')
            ->where('StrahovkaID', $id)
            ->get()->first();

        return $strahovka;

    }
}
